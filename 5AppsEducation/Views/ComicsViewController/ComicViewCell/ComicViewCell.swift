//
//  ComicViewCell.swift
//  5AppsEducation
//
//  Created by Max on 10.01.2022.
//

import Foundation
import UIKit

final class ComicViewCell: UITableViewCell {

    // MARK: - IBOutlet

    @IBOutlet weak var comicName: UILabel!
}
