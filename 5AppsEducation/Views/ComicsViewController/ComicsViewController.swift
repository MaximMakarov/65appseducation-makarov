//
//  ComicsViewController.swift
//  5AppsEducation
//
//  Created by Max on 10.01.2022.
//

import Foundation
import UIKit

class ComicsViewController: UIViewController, UITableViewDataSource {

    // MARK: - Properties

    var comics: [String]!

    // MARK: - IBOutlet

    @IBOutlet weak var tableView: UITableView!

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let comics = comics {
            return comics.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "ComicViewCell",
            for: indexPath) as? ComicViewCell
        else {
            return UITableViewCell()
        }
        cell.comicName.text = comics[indexPath.row]
        return cell
    }
}
