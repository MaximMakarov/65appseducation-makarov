import Foundation
import UIKit

class BaseViewController: UIViewController {

    func showView(name: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: name)
        self.show(nextViewController, sender: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        if self.restorationIdentifier == "AutorizationView" || self.restorationIdentifier == "MarvelViewController"{
            super.viewWillAppear(animated)
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if self.restorationIdentifier == "AutorizationView" || self.restorationIdentifier == "MarvelViewController"{
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            super.viewWillDisappear(animated)
        }
    }
}
