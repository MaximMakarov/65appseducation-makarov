//
//  AutorizationViewController.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation
import UIKit

class AutorizationViewController: BaseViewController, AutorizationViewControllerProtocol {

    // MARK: - Properties

    lazy private var presenter: AuthorizationPresenterProtocol = AuthorizationPresenter(
        autorizationViewController: self)

    // MARK: - IBOutlet

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!

    // MARK: - IBAction

    @IBAction func singInBtn(_ sender: Any) {
        presenter.authorization(email: email.text, password: password.text)
    }

    @IBAction func forgotPasswordBtn(_ sender: Any) {
        showView(name: "SendCodeView")
    }

    @IBAction func dontHaveAccBtn(_ sender: Any) {
        showView(name: "CreateAccountView")
    }

    // MARK: - AutorizationViewControllerProtocol

    func showErrorMsg(msg: String) {
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
