//
//  PasswordRecoveryViewController.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation
import UIKit

class PasswordRecoveryViewController: BaseViewController, PasswordRecoveryViewControllerProtocol {

    // MARK: - Properties

    lazy private var presenter: PasswordRecoveryPresenterProtocol = PasswordRecoveryPresenter(
        passwordRecoveryViewController: self)

    // MARK: - IBOutlet

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!

    // MARK: - IBAction

    @IBAction func saveNewPasswordBtn(_ sender: Any) {
    }

    // MARK: - PasswordRecoveryViewControllerProtocol

    func showErrorMsg(msg: String) {
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
