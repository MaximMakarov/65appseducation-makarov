//
//  CreateAccountViewController.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation
import UIKit

class CreateAccountViewController: BaseViewController, CreateAccountViewControllerProtocol {

    // MARK: - Properties

    lazy private var presenter: CreateAccountPresenterProtocol = CreateAccountPresenter(
        createAccountViewController: self)

    // MARK: - IBOutlet

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!

    // MARK: - IBAction

    @IBAction func createAccountBtn(_ sender: Any) {
        presenter.createAccount(
            name: name.text,
            email: email.text,
            password: password.text,
            confirmPassword: confirmPassword.text)
    }

    @IBAction func haveAccountBtn(_ sender: Any) {
        showView(name: "AutorizationView")
    }

    // MARK: - CreateAccountViewControllerProtocol

    func showErrorMsg(msg: String) {
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
