//
//  SendCodeViewController.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation
import UIKit

class SendCodeViewController: BaseViewController, SendCodeViewControllerProtocol {

    // MARK: - Properties

    lazy private var presenter: SendCodePresenterProtocol = SendCodePresenter(sendCodeViewController: self)

    // MARK: - IBOutlet

    @IBOutlet weak var email: UITextField!

    // MARK: - IBAction

    @IBAction func sendBtn(_ sender: Any) {
        presenter.sendCode(email: email.text)
    }

    // MARK: - SendCodeViewControllerProtocol

    func showErrorMsg(msg: String) {
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
