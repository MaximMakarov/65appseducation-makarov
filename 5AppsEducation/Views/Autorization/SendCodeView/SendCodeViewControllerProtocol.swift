//
//  SendCodeViewControllerProtocol.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

protocol SendCodeViewControllerProtocol: AnyObject {
    func showErrorMsg(msg: String)
    func showView(name: String)
}
