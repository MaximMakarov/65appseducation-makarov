import Foundation
import UIKit
import SweetLike

final class HeroViewCell: UITableViewCell {

    // MARK: - Properties

    var index: Int?
    var infoButtonAction: (() -> Void)?

    // MARK: - IBOutlet

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var sweetLikeButton: SweetLike!

    // MARK: - IBAction

    @IBAction func infoBtnClick(_ sender: Any) {
        infoButtonAction?()
    }

    func subcribe(infoButtonAction: @escaping () -> Void) {
        self.infoButtonAction = infoButtonAction
    }
}
