//
//  MarvelViewControllerProtocol.swift
//  5AppsEducation
//
//  Created by Max on 09.12.2021.
//

import Foundation

protocol MarvelViewControllerProtocol: AnyObject {
    func reloadData()
}
