//
//  MarvelViewController.swift
//  5AppsEducation
//
//  Created by Max on 09.12.2021.
//

import Foundation
import UIKit
import Kingfisher

class MarvelViewController: UIViewController, UITableViewDataSource, UITabBarDelegate, MarvelViewControllerProtocol {

    // MARK: - Properties

    lazy private var presenter: MarvelPresenterProtocol = MarvelPresenter(view: self)
    var isMoreMode = true {
        didSet {
            reloadData()
        }
    }

    // MARK: - IBOutlet

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var moreBtn: UITabBarItem!
    @IBOutlet weak var favoritesBtn: UITabBarItem!
    @IBOutlet weak var tabBar: UITabBar!

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tabBar.delegate = self
        presenter.requestData()
    }

    override func viewWillAppear(_ animated: Bool) {
        if self.restorationIdentifier == "MarvelViewController"{
            super.viewWillAppear(animated)
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }

    public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item === favoritesBtn {
            isMoreMode = false
        } else {
            isMoreMode = true
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if self.restorationIdentifier == "MarvelViewController"{
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            super.viewWillDisappear(animated)
        }
    }

    // MARK: - ImageTransform

    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        guard let imageView = sender.view as? UIImageView else {
            return
        }
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)

        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(startZooming(_:)))
        newImageView.addGestureRecognizer(pinchGesture)

        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }

    @objc
    private func startZooming(_ sender: UIPinchGestureRecognizer) {
        let scaleResult = sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)
        guard let scale = scaleResult, scale.a > 1, scale.d > 1 else { return }
        sender.view?.transform = scale
        sender.scale = 1
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "HeroViewCell",
            for: indexPath) as? HeroViewCell
        else {
            return UITableViewCell()
        }
        cell.index = indexPath.row
        let url = URL(string: presenter.heroLink(index: cell.index!, isMoreMode: isMoreMode))
        cell.img.kf.setImage(with: url)
        let pictureTap = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        cell.img.addGestureRecognizer(pictureTap)
        cell.name.text = presenter.heroName(index: cell.index!, isMoreMode: isMoreMode)
        cell.subcribe {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ComicsViewController")
            if let viewController = nextViewController as? ComicsViewController {
                viewController.comics =  self.presenter.comics(index: indexPath.row, isMoreMode: self.isMoreMode)
            }
            self.show(nextViewController, sender: self)
        }
        if presenter.isHeroLiked(index: cell.index!, isMoreMode: isMoreMode) {
            cell.sweetLikeButton.setStatus( .liked)
        } else {
            cell.sweetLikeButton.setStatus( .unliked)
        }
        cell.sweetLikeButton.likeAction = { [self] in
            self.presenter.heroLike(index: cell.index!, isMoreMode: isMoreMode)
        }
        cell.sweetLikeButton.unlikeAction = { [self] in
            self.presenter.heroUnLike(index: cell.index!, isMoreMode: isMoreMode)
        }
        if isMoreMode == false {
            cell.sweetLikeButton.isHidden = true
        } else {
            cell.sweetLikeButton.isHidden = false
        }
        if isMoreMode == false {
            cell.infoButton.isHidden = true
        } else {
            cell.infoButton.isHidden = false
        }
        tableView.rowHeight = view.frame.height - 100
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMoreMode {
            return presenter.rowCount(isMoreMode: isMoreMode)
        }
        return dataBaseModel.getLikeHeroCount()
    }

    // MARK: - MarvelViewControllerProtocol

    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
