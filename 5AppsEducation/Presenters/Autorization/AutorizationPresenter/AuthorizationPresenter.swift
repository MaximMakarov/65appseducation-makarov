//
//  AuthorizationPresenter.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

class AuthorizationPresenter: AuthorizationPresenterProtocol {

    // MARK: - Properties

    weak var view: AutorizationViewControllerProtocol?
    lazy var model: AuthorizationModelProtocol = AuthorizationModel(baseAutorizationPresenter: self)

    // MARK: - Init

    init(autorizationViewController: AutorizationViewControllerProtocol) {
        view = autorizationViewController
    }

    // MARK: - AuthorizationPresenterProtocol

    func showErrorMessage(msg: String) {
        view?.showErrorMsg(msg: msg)
    }

    func authorization(email: String?, password: String?) {
        if let email = email, let password = password {
            model.authorization(email: email, password: password)
        }
    }

    func showView(name: String) {
        view?.showView(name: name)
    }
}
