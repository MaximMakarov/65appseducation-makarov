//
//  AutorizationPresenterProtocol.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

protocol AuthorizationPresenterProtocol: BaseAutorizationPresenterProtocol {
    func authorization(email: String?, password: String?)
}
