//
//  SendCodePresenterProtocol.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

protocol SendCodePresenterProtocol: BaseAutorizationPresenterProtocol {
    func sendCode(email: String?)
}
