//
//  SendCodePresenter.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

class SendCodePresenter: SendCodePresenterProtocol {

    // MARK: - Properties

    unowned var view: SendCodeViewControllerProtocol?
    lazy var model: AuthorizationModelProtocol = AuthorizationModel(baseAutorizationPresenter: self)

    // MARK: - Init

    init(sendCodeViewController: SendCodeViewControllerProtocol) {
        view = sendCodeViewController
    }

    // MARK: - SendCodePresenterProtocol

    func sendCode(email: String?) {
        if let email = email {
            model.sendCode(email: email)
        }
    }

    // MARK: - BaseAutorizationPresenterProtocol

    func showErrorMessage(msg: String) {
        if let view = view {
            view.showErrorMsg(msg: msg)
        }
    }

    func showView(name: String) {
        if let view = view {
            view.showView(name: name)
        }
    }
}
