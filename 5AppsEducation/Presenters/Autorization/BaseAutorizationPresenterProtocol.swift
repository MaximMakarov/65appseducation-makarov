//
//  BaseAutorizationPresenterProtocol.swift
//  5AppsEducation
//
//  Created by Max on 02.01.2022.
//

import Foundation

protocol BaseAutorizationPresenterProtocol: AnyObject {
    func showErrorMessage(msg: String)
    func showView(name: String)
}
