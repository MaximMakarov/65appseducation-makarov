//
//  PasswordRecoveryPresenterProtocol.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

protocol PasswordRecoveryPresenterProtocol: BaseAutorizationPresenterProtocol {
    func saveNewPassword(password: String?, confirmPassword: String?)
}
