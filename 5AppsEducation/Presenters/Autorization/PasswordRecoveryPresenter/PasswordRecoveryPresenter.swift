//
//  PasswordRecoveryPresenter.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

class PasswordRecoveryPresenter: PasswordRecoveryPresenterProtocol {

    // MARK: - Properties

    unowned var view: PasswordRecoveryViewControllerProtocol?
    lazy var model: AuthorizationModelProtocol = AuthorizationModel(baseAutorizationPresenter: self)

    // MARK: - Init

    init(passwordRecoveryViewController: PasswordRecoveryViewControllerProtocol) {
        view = passwordRecoveryViewController
    }

    // MARK: - PasswordRecoveryPresenterProtocol

    func saveNewPassword(password: String?, confirmPassword: String?) {
        if let password = password, let confirmPassword = confirmPassword {
            if password == confirmPassword {
                model.saveNewPassword(password: password)
            }
        }
    }

    // MARK: - BaseAutorizationPresenterProtocol

    func showErrorMessage(msg: String) {
        if let view = view {
            view.showErrorMsg(msg: msg)
        }
    }

    func showView(name: String) {
        if let view = view {
            view.showView(name: name)
        }
    }
}
