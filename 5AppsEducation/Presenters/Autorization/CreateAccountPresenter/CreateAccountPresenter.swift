//
//  CreateAccountPresenter.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

class CreateAccountPresenter: CreateAccountPresenterProtocol {

    // MARK: - Properties

    unowned var view: CreateAccountViewControllerProtocol?
    lazy var model: AuthorizationModelProtocol = AuthorizationModel(baseAutorizationPresenter: self)

    // MARK: - Init

    init(createAccountViewController: CreateAccountViewControllerProtocol) {
        view = createAccountViewController
    }

    // MARK: - CreateAccountPresenterProtocol

    func createAccount(name: String?, email: String?, password: String?, confirmPassword: String?) {
        if let name = name, let email = email, let password = password, let confirmPassword = confirmPassword {
            if password == confirmPassword {
                model.createAccount(name: name, email: email, password: password)
            }
        }
    }

    // MARK: - BaseAutorizationPresenterProtocol

    func showErrorMessage(msg: String) {
        if let view = view {
            view.showErrorMsg(msg: msg)
        }
    }

    func showView(name: String) {
        if let view = view {
            view.showView(name: name)
        }
    }
}
