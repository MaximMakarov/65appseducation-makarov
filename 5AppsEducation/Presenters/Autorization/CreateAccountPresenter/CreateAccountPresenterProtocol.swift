//
//  CreateAccountPresenterProtocol.swift
//  5AppsEducation
//
//  Created by Max on 06.12.2021.
//

import Foundation

protocol CreateAccountPresenterProtocol: BaseAutorizationPresenterProtocol {
    func createAccount(name: String?, email: String?, password: String?, confirmPassword: String?)
}
