//
//  MarvelPresenterProtocol.swift
//  5AppsEducation
//
//  Created by Max on 09.12.2021.
//

import Foundation
import UIKit

protocol MarvelPresenterProtocol: AnyObject {
    func requestData()
    func rowCount(isMoreMode: Bool) -> Int
    func heroName(index: Int, isMoreMode: Bool) -> String
    func heroLink(index: Int, isMoreMode: Bool) -> String
    func heroId(index: Int, isMoreMode: Bool) -> Int
    func heroLike(index: Int, isMoreMode: Bool)
    func heroUnLike(index: Int, isMoreMode: Bool)
    func isHeroLiked(index: Int, isMoreMode: Bool) -> Bool
    func comics(index: Int, isMoreMode: Bool) -> [String]
    func reloadData()
}
