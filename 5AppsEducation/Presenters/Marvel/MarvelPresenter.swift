//
//  MarvelPresenter.swift
//  5AppsEducation
//
//  Created by Max on 09.12.2021.
//

import Foundation
import UIKit
import Kingfisher
import SwiftUI

class MarvelPresenter: MarvelPresenterProtocol {

    // MARK: - Properties

    unowned var view: MarvelViewControllerProtocol?
    lazy var model: MarvelDataModelProtocol = MarvelDataModel(marvelPresenter: self)

    var description: String = ""

    init(view: MarvelViewControllerProtocol) {
        self.view = view
    }

    // MARK: - MarvelPresenterProtocol

    func rowCount(isMoreMode: Bool) -> Int {
        return model.marvelData?.data.results.count ?? 0
    }

    func heroName(index: Int, isMoreMode: Bool) -> String {
        if isMoreMode {
            return model.marvelData?.data.results[index].name ?? "no name"
        } else {
            return dataBaseModel.getHeroName(index: index)
        }
    }

    func heroLink(index: Int, isMoreMode: Bool) -> String {
        if isMoreMode {
            let path = model.marvelData?.data.results[index].thumbnail.path ?? ""
            let ext = model.marvelData?.data.results[index].thumbnail.ext ?? ""
            return ( path + "." + ext)
        } else {
            return dataBaseModel.getHeroLink(index: index)
        }
    }

    func heroId(index: Int, isMoreMode: Bool) -> Int {
        if isMoreMode {
            return model.marvelData?.data.results[index].id ?? 0
        } else {
            return 0
        }
    }

    func heroLike(index: Int, isMoreMode: Bool) {
        if isMoreMode {
            dataBaseModel.addHero(heroId: String(heroId(index: index, isMoreMode: true)),
                                  heroName: heroName(index: index, isMoreMode: true),
                                  imgUrl: heroLink(index: index, isMoreMode: true))
        }
    }
    func heroUnLike(index: Int, isMoreMode: Bool) {
        if isMoreMode {
            dataBaseModel.removeHero(heroId: String(heroId(index: index, isMoreMode: true)))
        }
    }

    func isHeroLiked(index: Int, isMoreMode: Bool) -> Bool {
        return dataBaseModel.isHeroLiked(heroId: String(heroId(index: index, isMoreMode: isMoreMode)))
    }

    func comics(index: Int, isMoreMode: Bool) -> [String] {
        var result: [String] = []
        if isMoreMode {
            if let items = model.marvelData?.data.results[index].comics.items {
                for item in  items {
                    result.append(item.name ?? "")
                }
            }
        }
        return result
    }

    func requestData() {
        model.requestData()
    }

    // MARK: - ReceiverProtocol

    func reloadData() {
        if let view = view {
            view.reloadData()
        }
    }
}
