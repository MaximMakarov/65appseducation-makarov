//
//  DataBaseModel.swift
//  5AppsEducation
//
//  Created by Max on 11.01.2022.
//

import Foundation
import FirebaseDatabase

var dataBaseModel: DataBaseModelProtocol = DataBaseModel()

class DataBaseModel: DataBaseModelProtocol {

    // MARK: - Properties

    private var heroes: [String: [String: String]] = [:]
    private var isLoad = false
    private var refHandle: UInt!
    var ref: DatabaseReference!

    init() {
        ref = Database.database().reference()
    }

    // MARK: - DataBaseModelProtocol

    var userName: String  = ""

    var userId: String  = "" {
        didSet {
            if let ref = ref {
                ref.child(userId)
                if userName != "" {
                    ref.child(userId).child("name").setValue(userName)
                }
                ref.child(userId).child("heroes")
                refHandle =  ref.child(userId).child("heroes").observe( .value, with: { snapshot in
                    self.heroes = snapshot.value as? [String: [String: String]] ?? [:]
                })
            }
        }
    }

    func addHero(heroId: String, heroName: String, imgUrl: String) {
        ref.child(userId).child("heroes").child(heroId).child("name").setValue(heroName)
        ref.child(userId).child("heroes").child(heroId).child("url").setValue(imgUrl)
    }

    func getLikeHeroCount() -> Int {
        return heroes.count
    }

    func getHeroName(index: Int) -> String! {
        return heroes[Array(heroes.keys)[index]]?["name"]! ?? ""
    }

    func getHeroLink(index: Int) -> String! {
        return heroes[Array(heroes.keys)[index]]?["url"]! ?? ""
    }

    func removeHero(heroId: String) {
        ref.child(userId).child("heroes").child(heroId).removeValue()
    }

    func isHeroLiked(heroId: String) -> Bool {
        if heroes[heroId] == nil {
            return false
        }
        return true
    }
}
