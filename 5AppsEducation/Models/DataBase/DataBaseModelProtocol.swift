//
//  DataBaseModelProtocol.swift
//  5AppsEducation
//
//  Created by Max on 11.01.2022.
//

import Foundation

protocol DataBaseModelProtocol: AnyObject {
    var userName: String {get set}
    var userId: String {get set}
    func addHero(heroId: String, heroName: String, imgUrl: String)
    func getLikeHeroCount() -> Int
    func getHeroName(index: Int) -> String!
    func getHeroLink(index: Int) -> String!
    func removeHero(heroId: String)
    func isHeroLiked(heroId: String) -> Bool
}
