//
//  AuthorizationModel.swift
//  5AppsEducation
//
//  Created by Max on 23.11.2021.
//

import Foundation
import Firebase
import FirebaseDatabase

class AuthorizationModel: AuthorizationModelProtocol {

    // MARK: - Properties

    unowned var presenter: BaseAutorizationPresenterProtocol?
    var user: User?

    // MARK: - Init

    init(baseAutorizationPresenter: BaseAutorizationPresenterProtocol) {
        presenter = baseAutorizationPresenter
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
    }

    // MARK: - AuthorizationProtocol

    func authorization(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            guard let curUser = result?.user, error == nil else {
                self.presenter?.showErrorMessage(msg: error!.localizedDescription)
                return
            }
            self.user = curUser
            self.initialization()
            self.presenter?.showView(name: "MarvelViewController")
        }
    }

    func  initialization() {
        dataBaseModel.userId = user!.uid
    }

    func sendCode(email: String) {
    }

    func createAccount(name: String, email: String, password: String) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            guard let curUser = result?.user, error == nil else {
                self.presenter?.showErrorMessage(msg: error!.localizedDescription)
                return
            }
            self.user = curUser
            self.initialization()
            self.presenter?.showView(name: "MarvelViewController")
        }
    }

    func saveNewPassword(password: String) {
    }
}
