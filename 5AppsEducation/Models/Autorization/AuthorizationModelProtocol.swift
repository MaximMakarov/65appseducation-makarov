//
//  AuthorizationProtocol.swift
//  5AppsEducation
//
//  Created by Max on 23.11.2021.
//

import Foundation

protocol AuthorizationModelProtocol: AnyObject {
    func authorization(email: String, password: String)
    func sendCode(email: String)
    func createAccount(name: String, email: String, password: String)
    func saveNewPassword(password: String)
}
