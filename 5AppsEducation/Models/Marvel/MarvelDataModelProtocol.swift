//
//  MarvelDataModelProtocol.swift
//  5AppsEducation
//
//  Created by Max on 11.01.2022.
//

import Foundation
import CodableModels

protocol MarvelDataModelProtocol: AnyObject {
    var marvelData: CodableModel? {get}
    func requestData()
}
