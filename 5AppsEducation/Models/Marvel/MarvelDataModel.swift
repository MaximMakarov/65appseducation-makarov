//
//  MarvelDataModel.swift
//  5AppsEducation
//
//  Created by Max on 11.01.2022.
//

import Foundation
import Networking
import CodableModels

class MarvelDataModel: MarvelDataModelProtocol {

    // MARK: - Properties

    private lazy var service = MarvelCharacterNetworkService()
    var marvelData: CodableModel?
    unowned var presenter: MarvelPresenterProtocol?

    // MARK: - Init

    init(marvelPresenter: MarvelPresenterProtocol) {
        presenter = marvelPresenter
    }

    // MARK: - ProcessingData

    private func processingData(data: Data) {
        let decoder = JSONDecoder()
        do {
            let result = try decoder.decode(CodableModel.self, from: data)
            marvelData = result
            if let presenter = presenter {
                presenter.reloadData()
            }
        } catch DecodingError.keyNotFound(let key, let context) {
            Swift.print("could not find key \(key) in JSON: \(context.debugDescription)")
        } catch DecodingError.valueNotFound(let type, let context) {
            Swift.print("could not find type \(type) in JSON: \(context.debugDescription)")
        } catch DecodingError.typeMismatch(let type, let context) {
            Swift.print("type mismatch for type \(type) in JSON: \(context.debugDescription)")
        } catch DecodingError.dataCorrupted(let context) {
            Swift.print("data found to be corrupted in JSON: \(context.debugDescription)")
        } catch let error as NSError {
            NSLog("Error in read(from:ofType:) domain= \(error.domain)," +
                  "description= \(error.localizedDescription)")
        }
    }

    private func processingError(error: Error) {
    }

    private func processingResponce(responce: URLResponse) {
    }

    func requestData() {
        service.fetchMarvel(
            processinData: processingData,
            processingResponse: processingResponce,
            processingError: processingError
        )
    }
}
