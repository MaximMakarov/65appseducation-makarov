import Foundation

public struct CodableModel: Decodable {
    public let code: Int?
    public let status: String?
    public let copyright: String?
    public let attributionText: String?
    public let attributionHTML: String?
    public let etag: String?
    public let data: ResponseData
}

public struct ResponseData: Decodable {
    public let offset: Int?
    public let limit: Int?
    public let total: Int?
    public let count: Int?
    public let results: [ResponseResult]
}

public struct ResponseResult: Decodable {
    public let id: Int?
    public let name: String?
    public let description: String?
    public let modified: String?
    public let thumbnail: Thumbnail
    public let resourceURI: String?
    public let comics: Comics
    public let series: Series
    public let stories: Stories
    public let events: Events
    public let urls: [Url]
}

public struct Events: Decodable {
    public let available: Int?
    public let collectionURI: String?
    public let items: [Item]
    public let returned: Int?
}

public struct Url: Decodable {
    public let type: String?
    public let url: String?
}

public struct Series: Decodable {
    public let available: Int?
    public let collectionURI: String?
    public let items: [StoriesItem]
    public let returned: Int?
}

public struct Stories: Decodable {
    public let available: Int?
    public let collectionURI: String?
    public let items: [StoriesItem]
    public let returned: Int?
}

public struct StoriesItem: Decodable {
    public let resourceURI: String?
    public let name: String?
    public let type: String?
}

public struct Thumbnail: Decodable {
    public let path: String?
    public let ext: String?

    public enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}

public struct Comics: Decodable {
    public let available: Int?
    public let collectionURI: String?
    public let items: [Item]
    public let returned: Int?
}

public struct Item: Decodable {
    public let resourceURI: String?
    public let name: String?
}
