Pod::Spec.new do |spec|
    spec.name           = "CodableModels"
    spec.version        = "0.0.1"
    spec.summary        = "CodableModels"
    spec.description    = <<-DESC
    Codable Models
                    DESC
    spec.homepage       = "https://65apps.com"
    spec.license        = "BSD"
    spec.author          = { "Makarov" => "makarov@mail.ru"}
    spec.platform       = :ios, "13.0"
    spec.swift_version  = "5.0"
    spec.source         = { :path => "."}
    spec.source_files = "CodableModels/**/*.{h,m,swift,xib,storyboard}"
end
