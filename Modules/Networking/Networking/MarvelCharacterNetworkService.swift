import Foundation
import CryptoKit

public class MarvelCharacterNetworkService {

    // MARK: - Properties

    private let configuration: URLSessionConfiguration = .default
    private lazy var urlSession: URLSession = .init(configuration: configuration)
    private let publicKey = "a95fc867f5c36770fd1d768b0f866245"
    private let privateKey = "9b2dada0dcf956d08ae3898b9e1ded76b7c36df5"

    public init() {}
    private var timeSpan: Int {
            return Int(Date().timeIntervalSince1970)
    }

    private func marvelUrl(limit: Int, offset: Int) -> String {
        return "https://gateway.marvel.com/v1/public/characters?ts=" +
        "\(timeSpan)&orderBy=name&limit=\(limit)&offset=\(offset)&apikey=\(publicKey)&" +
        "hash=\(MD5(string: String(timeSpan) + privateKey + publicKey))"
    }

    // MARK: - Cripto

    private func MD5(string: String) -> String {
        let digest = Insecure.MD5.hash(data: string.data(using: .utf8) ?? Data())
        return digest.map {
            String(format: "%02hhx", $0)
        }.joined()
    }

    // MARK: - Marvel api

    public func fetchMarvel(processinData: @escaping (Data) -> Void,
                            processingResponse: @escaping (URLResponse) -> Void,
                            processingError: @escaping (Error) -> Void) {
        let request: URLRequest = .init(url: URL(string: marvelUrl(limit: 100, offset: 0))!)
        let dataTask = urlSession.dataTask(with: request) { data, response, error in
            if let error = error {
                processingError(error)
                return
            }
            guard let response = response as? HTTPURLResponse,
                        (200...299).contains(response.statusCode) else {
                            processingResponse(response!)
                        return
                    }
            if let data = data {
                processinData(data)
            }
        }
        dataTask.resume()
    }
}
