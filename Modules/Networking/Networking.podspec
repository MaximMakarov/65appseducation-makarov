Pod::Spec.new do |spec|
    spec.name           = "Networking"
    spec.version        = "0.0.1"
    spec.summary        = "Networking"
    spec.description    = <<-DESC
    Codable Models
                    DESC
    spec.homepage       = "https://65apps.com"
    spec.license        = "BSD"
    spec.author          = { "Makarov" => "makarov@mail.ru"}
    spec.platform       = :ios, "13.0"
    spec.swift_version  = "5.0"
    spec.source         = { :path => "."}
    spec.source_files = "Networking/*.{h,m,swift,xib,storyboard}"
    spec.dependency     "CodableModels"
end
